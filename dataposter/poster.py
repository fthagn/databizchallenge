import requests as rq
import time
import pandas as pd
import json

filename = "hackathon_EnBW_smart_meter_data_30_hh.csv"
url = "https://databiz.herokuapp.com/energy_data/post"

auth_tokens = [
 "31bbd65412b74385d46cf9b062d72ccce6961ac3",
 "9ba5a3158e6df6ba51fbf20eb544d147a8d10e38",
 "8b8b6ae590cead728051f96adad76a01d151cc4a",
 "e3ca6a1ebd389fda85f148eb4d760452219a2c60",
 "dae9c0764be09e34f423781748434ec0e9484abb",
 "5596eff34b21031c00660c033ef8ec8fbc61d7f6",
 "594af9c0d64683d73a67bed6164eba9b051a6d1a",
 "2c49bd10ce6fafc23228d18b951e076992a863e6",
 "35d34eea8590c8256efd88cb06840657af01c5dc",
 "bc18a37b3cd9c81b4cf5391652b5a32b39a08ca5",
 "cc8eb15bbbcb5dc5a6de86b9e08242b0f90ae0f7",
 "2aa99ea8b42e7d2fec06d747073314efff877d0e",
 "683e775e730bbf9c67af32fee60c3d3738fc1f99",
 "37b86970eba27bee586a098191373d855d6136f9",
 "68f4ac164cb76980bbd92274946179a7c9469ac5",
 "b1ba3a31558fda1cdaaba39db9b988ff25089b00",
 "c88fd372bd2f93bd9e0006120146502fad6423ca",
 "6a27ab2c7c03c585b6637e8e7f50fae078266915",
 "8314ca3805c88e6f23c53b3fce01bfc03909dfed",
 "3b4ac0750ec2ffaae7e88b683d867b7a82a4f775",
 "e4443306c649f6cb4fb468ded5ac792fbe87c69c",
 "10ed5bd4b72a7a41e5ee9a088936f1739e799bf2",
 "7abfff5c9802cfe637dace52fb9ab34d50f375ce",
 "ad1bc931d1fabf2380672c1e5a17f45e1edd6ea9",
 "598b6364ae36c3b42ed52dfc8cb9f4968c17955d",
 "c6a1a54272aef231f69b9e967a545b19b9cad020",
 "cfc8a492f0c711260e3991ef1fba204cb012fc58",
 "29214c74361dba578309ee2f1b7b45cc6dfd97c9",
 "af10cce82bef97255b1d9a56c6b3ca58faa1a342",
 "f6bc08bc02ac775b3a7b30f535fc17ae8f4c6836",
 "5c75d47604f644aa8871bdebd65c16f542af04f1",
 "9f31c40a657bc1af861408b5446690128f1d785e",
 "8573f66f8a4f14e8c4dba2c691ad8fcfc981b0e0",
 "b9e3fa0f2f92f301fb9e91532dab09459395fad5",
 "ed8d52bb2f1a36b1a0389c78bd37757c87dfda00"
]

df = pd.read_csv(filename, sep=';', index_col=0, decimal=',')
dfwide = df.pivot_table(index='timestampLocal', columns='id', values='value')
ids = df['id'].unique()
print("id's:", ids)
for i in range(len(dfwide)):
    row = dfwide.iloc[i]
    ts = row.name
    print(ts)
    # print(row)
    # print(row.index)
    for _id in ids:
        print(_id, row[_id])
        headers = {
            'content-type': 'application/json',
            'Authorization': "Token " + auth_tokens[_id]
        }
        payload = {
            'timestamp': ts,
            'value': row[_id]
        }
        rq.post(url, data=json.dumps(payload), headers=headers)
    # time.sleep(1)
